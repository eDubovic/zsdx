﻿using UnityEngine;

namespace Assets.Zsdx.Scripts.Lib {
    public class GameObjectArguments {
        public string Name { get; set; }
        public Sprite Sprite { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public int Layer { get; set; }
        public Transform ParentTransform { get; set; }
    }
}
