﻿using System;
using Assets.Zsdx.Scripts.Model;
using UnityEngine;
using UnityEngine.Experimental.Rendering;
using MapEntity = Assets.Zsdx.Scripts.Entity.Map.Map;
using TileEntity = Assets.Zsdx.Scripts.Entity.Map.Tile;
using TeletransporterEntity = Assets.Zsdx.Scripts.Entity.Map.Teletransporter;
using ChestEntity = Assets.Zsdx.Scripts.Entity.Map.Chest;
using DestructibleEntity = Assets.Zsdx.Scripts.Entity.Map.Destructible;

using TilesetEntity = Assets.Zsdx.Scripts.Entity.Tileset.Tileset;
using TilePatternEntity = Assets.Zsdx.Scripts.Entity.Tileset.TilePattern;

namespace Assets.Zsdx.Scripts.Lib {
    public class Map {

        private readonly GameObject _worldMap;

        public Map() {
            this._worldMap = new GameObject("WorldMap");
        }

        public void CreateMap(MapEntity map, TilesetEntity tileset) {
            for (int i = map.Destructible.Count - 1; i >= 0; i--) {
                this.CreateDestructible(map.Destructible[i]);
            }
            for (int i = map.Chest.Count - 1; i >= 0; i--) {
                this.CreateChest(map.Chest[i]);
            }
            for (int i = map.Teletransporter.Count - 1; i >= 0; i--) {
                this.CreateTeletransporter(map.Teletransporter[i]);
            }
            for (int i = map.Tile.Count - 1; i >= 0; i--) {
                TileEntity tile = map.Tile[i];
                TilePatternEntity tilePattern = tileset.TilePattern.Find(x => x.Id.Equals(tile.Pattern));
                SpriteArguments spriteArguments = new SpriteArguments {
                    Texture2D = tileset.Texture2D,
                    X = tilePattern.X[0],
                    Y = tilePattern.Y[0],
                    Width = tilePattern.Width,
                    Height = tilePattern.Height,
                };
                Sprite sprite = this.CreateSprite(spriteArguments);
                TileArguments tileArguments = new TileArguments() {
                    Tile = tile,
                    Sprite = sprite,
                    Width = tilePattern.Width,
                    Height = tilePattern.Height
                };
                this.CreateTitle(tileArguments);
            }
        }


        public void CreateTitle(TileArguments arguments) {
            int repeatByWidth = Mathf.Abs(arguments.Tile.Width / arguments.Width);
            int repeatByHeight = Mathf.Abs(arguments.Tile.Height / arguments.Height);

            GameObjectArguments gameObjectArguments = new GameObjectArguments {
                Name = "Title",
                X = arguments.Tile.X,
                Y = arguments.Tile.Y,
                Layer = arguments.Tile.Layer,
                Sprite = arguments.Sprite,
                ParentTransform = this._worldMap.transform
            };
            if (repeatByWidth == 1 && repeatByHeight == 1) {
                this.CreateGameObject(gameObjectArguments);
            } else if (repeatByWidth > 1 && repeatByHeight == 1) {
                float x = arguments.Tile.X;
                for (int i = 0; i < repeatByWidth; i++) {
                    gameObjectArguments.X = x;
                    this.CreateGameObject(gameObjectArguments);
                    x += arguments.Width;
                }
            } else if (repeatByWidth == 1 && repeatByHeight > 1) {
                float y = arguments.Tile.Y;
                for (int i = 0; i < repeatByHeight; i++) {
                    gameObjectArguments.Y = y;
                    this.CreateGameObject(gameObjectArguments);
                    y += arguments.Height;
                }
            } else if (repeatByWidth > 1 && repeatByHeight > 1) {
                float x = arguments.Tile.X;
                float y = arguments.Tile.Y;
                for (int i = 0; i < repeatByWidth; i++) {
                    for (int j = 0; j < repeatByHeight; j++) {
                        gameObjectArguments.X = x;
                        gameObjectArguments.Y = y;
                        this.CreateGameObject(gameObjectArguments);
                        y += arguments.Height;
                    }
                    y = arguments.Tile.Y;
                    x += arguments.Width;
                }
            }
        }

        public void CreateDestructible(DestructibleEntity destructible) {
            Texture2D tex = new Texture2D(1000, 1000);
            SpriteArguments spriteArguments = new SpriteArguments {
                Texture2D = tex,
                X = destructible.X,
                Y = destructible.Y,
                Width = 10,
                Height = 10,
            };
            Sprite sprite = this.CreateSprite(spriteArguments);

            GameObjectArguments gameObjectArguments = new GameObjectArguments {
                Name = "Destructible",
                X = destructible.X,
                Y = destructible.Y,
                Layer = 5,
                Sprite = sprite,
                ParentTransform = this._worldMap.transform
            };
            this.CreateGameObject(gameObjectArguments);
        }

        public void CreateChest(ChestEntity chest) {
            Sprite[] chestSprites = Resources.LoadAll<Sprite>("chest");
            Sprite sprite = Array.Find(chestSprites, s => s.name == "chest_closed");

            GameObjectArguments gameObjectArguments = new GameObjectArguments {
                Name = "Chest",
                X = chest.X,
                Y = chest.Y,
                Layer = 5,
                Sprite = sprite,
                ParentTransform = this._worldMap.transform
            };
            GameObject gameObject = this.CreateGameObject(gameObjectArguments);
            Animator animator = gameObject.AddComponent<Animator>();
            animator.runtimeAnimatorController = Resources.Load<RuntimeAnimatorController>("ChestController");
            
            gameObject.AddComponent<BoxCollider2D>();

            gameObject.AddComponent<ChestModel>();
            ChestModel chestModel = gameObject.GetComponent<ChestModel>();
            chestModel.Name = chest.Name;
            chestModel.X = chest.X;
            chestModel.Y = chest.Y;
            chestModel.TreasureName = chest.TreasureName;
            chestModel.TreasureVariant = chest.TreasureVariant;
            chestModel.TreasureSaveGameVariable = chest.TreasureSaveGameVariable;
            chestModel.OpeningCondition = chest.OpeningCondition;
            chestModel.OpeningConditionConsumed = chest.OpeningConditionConsumed;
            chestModel.CannotOpenDialog = chest.CannotOpenDialog;

        }

        public void CreateTeletransporter(TeletransporterEntity teletransporter) {
            Texture2D tex = new Texture2D(1000, 1000);
            SpriteArguments spriteArguments = new SpriteArguments {
                Texture2D = tex,
                X = teletransporter.X,
                Y = teletransporter.Y,
                Width = teletransporter.Width,
                Height = teletransporter.Height,
            };
            Sprite sprite = this.CreateSprite(spriteArguments);

            GameObjectArguments gameObjectArguments = new GameObjectArguments {
                Name = "Teletransporter",
                X = teletransporter.X,
                Y = teletransporter.Y,
                Layer = 5,
                Sprite = sprite,
                ParentTransform = this._worldMap.transform
            };
            this.CreateGameObject(gameObjectArguments);
        }

        private GameObject CreateGameObject(GameObjectArguments arguments) {
            GameObject gameObject = new GameObject(arguments.Name);
            gameObject.transform.parent = arguments.ParentTransform;
            gameObject.transform.position = new Vector2((arguments.X / 100), -(arguments.Y / 100));

            if (arguments.Sprite != null) {
                gameObject.AddComponent<SpriteRenderer>();
                SpriteRenderer spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
                spriteRenderer.sprite = arguments.Sprite;
                spriteRenderer.sortingOrder = arguments.Layer;
            }

            return gameObject;
        }

        private Sprite CreateSprite(SpriteArguments arguments) {
            return Sprite.Create(
                arguments.Texture2D, new Rect(arguments.X, arguments.Texture2D.height - arguments.Y - arguments.Height, arguments.Width, arguments.Height), new Vector2(0, 1f)
            );
        }


    }
}
