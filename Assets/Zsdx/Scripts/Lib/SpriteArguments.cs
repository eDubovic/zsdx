﻿using UnityEngine;

namespace Assets.Zsdx.Scripts.Lib {
    public class SpriteArguments  {
        public Texture2D Texture2D { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
