﻿using UnityEngine;
using TileEntity = Assets.Zsdx.Scripts.Entity.Map.Tile;

namespace Assets.Zsdx.Scripts.Lib {
    public class TileArguments {
        public Sprite Sprite { get; set; }
        public TileEntity Tile { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}
