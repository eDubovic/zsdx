﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using LitJson;

using MapEntity = Assets.Zsdx.Scripts.Entity.Map.Map;
using TilesetEntity = Assets.Zsdx.Scripts.Entity.Tileset.Tileset;

namespace Assets.Zsdx.Scripts {
    public class ManagerTileset {

        private const int CountTile = 19;
        private readonly List<TilesetEntity> _tileset;

        public ManagerTileset() {
            this._tileset = new List<TilesetEntity>();
            this.LoadTileset();
        }

        public TilesetEntity FindTilesetByMap(MapEntity map) {
            return this._tileset.Find(x => x.Id.Equals(map.Properties.Tileset));
        }

        private void LoadTileset() {
            for (int i = 0; i <= CountTile; i++) {
                string path = string.Format("{0}{1}", Application.dataPath, string.Format("/Resources/tilesets/{0}.json", i));
                if (!File.Exists(path)) {
                    continue;
                }
                TilesetEntity tileset = JsonMapper.ToObject<TilesetEntity>(File.ReadAllText(path));
                tileset.Texture2D = Resources.Load<Texture2D>(string.Format("tilesets/{0}.tiles", tileset.Id));
                this._tileset.Add(tileset);
            }
        }
    }
}
