﻿namespace Assets.Zsdx.Scripts.Equipment {
    public class Shield: Core.Equipment {
        public static Shield None() {
            return new Shield { Name = "None" };
        }

        public Shield Blue() {
            return new Shield() {
                Name = "Blue"
            };
        }

        public Shield Red() {
            return new Shield() {
                Name = "Red"
            };
        }

        public Shield Yellow() {
            return new Shield() {
                Name = "Yellow"
            };
        }
    }
}
