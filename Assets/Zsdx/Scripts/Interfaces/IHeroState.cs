﻿using Assets.Zsdx.Scripts.View.Hero;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Interfaces {
    public interface IHeroState {
        void Execude();
        void Enter(HeroView heroView);
        void Exit();
        void OnTriggerEnter(Collider2D collider2D);
    }
}
