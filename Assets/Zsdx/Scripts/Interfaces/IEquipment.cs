﻿namespace Assets.Zsdx.Scripts.Interfaces {
    public interface IEquipment {
        string Name { get; set; }
    }
}
