﻿namespace Assets.Zsdx.Scripts.Interfaces {
    public interface IItem {
        string Name { get; }
        string Description { get; }
        int RemainingUses { get; }
        bool Use();
    }
}
