﻿namespace Assets.Zsdx.Scripts.Interfaces {
    public interface IMovementType {
        float Speed();
        float Angle();
        float MaxDistance();
        float IsSmooth();
    }
}
