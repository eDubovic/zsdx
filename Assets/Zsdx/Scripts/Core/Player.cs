﻿using Assets.Zsdx.Scripts.Core.HeroState;
using Assets.Zsdx.Scripts.Interfaces;
using Assets.Zsdx.Scripts.View.Hero;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core {
    public class Player: MonoBehaviour {

        public Animator Animator { get; private set; }
        private IHeroState _currentHeroState;

        private void Start() {
            this.Animator = this.GetComponent<Animator>();
            this.ChangeState(new StoppedState());
        }

        private void Update() {
            this._currentHeroState.Execude();
        }

        

        public void ChangeState(IHeroState newHeroState) {
            if (this._currentHeroState != null) {
                this._currentHeroState.Exit();
            }
            this._currentHeroState = newHeroState;
            //this._currentHeroState.Enter(this);
        }
    }
}
