﻿using UnityEngine;

namespace Assets.Zsdx.Scripts.Core {
    public class HeroArgs {
        public Transform Transform { get; set; }
        public float WalkingSpeed { get; set; }
        public Direction Direction { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
    }
}
