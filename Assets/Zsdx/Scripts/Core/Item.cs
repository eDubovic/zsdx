﻿using Assets.Zsdx.Scripts.Interfaces;

namespace Assets.Zsdx.Scripts.Core {
    public class Item: IItem {
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public int RemainingUses { get; protected set; }

        public bool Use() {
            return this.UseItem();
        }

        protected virtual bool UseItem() {
            return false;
        }
    }
}
