﻿using Assets.Zsdx.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core.Movements {
    public class Straight: MovementType {

        public override void Move(HeroArgs heroArgs) {

            heroArgs.Transform.position += new Vector3(heroArgs.X, heroArgs.Y).normalized * Time.deltaTime;
        }
    }
}