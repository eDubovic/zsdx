﻿using UnityEngine;

namespace Assets.Zsdx.Scripts.Core.Movements {
    public abstract class MovementType {
        public float WalkingSpeed { get; set; }
        public float Angle { get; set; }
        public float MaxDistance { get; set; }
        public bool IsSmooth { get; set; }

        public abstract void Move(HeroArgs heroArgs);
    }
}
