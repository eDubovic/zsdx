﻿using Assets.Zsdx.Scripts.ZeldaApplication;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core.Movements {
    public class Movement {
        private readonly MovementType _movementType;

        public Movement(MovementType movementType) {
            this._movementType = movementType;
        }

        public void Start(HeroArgs heroArgs) {
            this._movementType.Move(heroArgs);
        }

        public void Stop() {

        }
    }
}
