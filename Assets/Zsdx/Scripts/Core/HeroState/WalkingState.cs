﻿using Assets.Zsdx.Scripts.Interfaces;
using Assets.Zsdx.Scripts.View.Hero;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core.HeroState {
    public class WalkingState: IHeroState {

        private HeroView _heroView;

        public void Execude() {
            if (!this._heroView.app.model.Hero.IsWalking) {
                this._heroView.ChangeState(new StoppedState());
            } else if (this._heroView.app.model.Hero.IsDiagonal) {
                this._heroView.ChangeState(new WalkingDiagonalState());
            }
        }

        public void Enter(HeroView heroView) {
            this._heroView = heroView;

            this._heroView.Animator.SetBool("IsWalking", this._heroView.app.model.Hero.IsWalking);
            this._heroView.Animator.SetFloat("Horizontal", this._heroView.app.model.Hero.Horizontal);
            this._heroView.Animator.SetFloat("Vertical", this._heroView.app.model.Hero.Vertical);
        }

        public void Exit() {
        }

        public void OnTriggerEnter(Collider2D collider2D) {
        }

    }
}
