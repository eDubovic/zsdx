﻿using Assets.Zsdx.Scripts.Interfaces;
using Assets.Zsdx.Scripts.View.Hero;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core.HeroState {
    public class WalkingDiagonalState: IHeroState {

        private HeroView _heroView;

        public void Execude() {
            if (!this._heroView.app.model.Hero.IsWalking) {
                this._heroView.ChangeState(new StoppedState());
            } else if (!this._heroView.app.model.Hero.IsDiagonal) {
                this._heroView.ChangeState(new WalkingState());
            }
        }

        public void Enter(HeroView heroView) {
            this._heroView = heroView;

            this._heroView.Animator.SetBool("IsDiagonal", this._heroView.app.model.Hero.IsDiagonal);
            this._heroView.Animator.SetFloat("Horizontal", this._heroView.app.model.Hero.Horizontal);
            this._heroView.Animator.SetFloat("Vertical", this._heroView.app.model.Hero.Vertical);
        }

        public void Exit() {
            this._heroView.Animator.SetBool("IsDiagonal", this._heroView.app.model.Hero.IsDiagonal);
        }

        public void OnTriggerEnter(Collider2D collider2D) {
        }
    }
}
