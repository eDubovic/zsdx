﻿using Assets.Zsdx.Scripts.Interfaces;
using Assets.Zsdx.Scripts.View.Hero;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core.HeroState {
    public class StoppedState: IHeroState {

        private HeroView _heroView;

        public void Execude() {
            if (this._heroView.app.model.Hero.IsWalking) {
                this._heroView.ChangeState(new WalkingState());
            }
        }

        public void Enter(HeroView heroView) {
            this._heroView = heroView;
            this._heroView.Animator.SetBool("IsWalking", this._heroView.app.model.Hero.IsWalking);
        }

        public void Exit() {
        }

        public void OnTriggerEnter(Collider2D collider2D) {
        }
    }
}
