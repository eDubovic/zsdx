﻿namespace Assets.Zsdx.Scripts.Core.HeroState {
    public class HeroNotifications {

        public const string WalkingAnimatorExit= "walking@animator-exit";

        public const string StoppedAnimatorBegin= "stopped@animator-begin";
        public const string StoppedAnimatorEnter= "stopped@animator-enter";
    }
}
