﻿using Assets.Zsdx.Scripts.Interfaces;

namespace Assets.Zsdx.Scripts.Core {
    public class Equipment: IEquipment {
        public string Name { get; set; }
    }
}
