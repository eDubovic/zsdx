﻿namespace Assets.Zsdx.Scripts.Core {
    public enum Direction {
        //East (восток) is 0, North (север) is 1, West (запад) is 2, South (юг) is 3.
        East, North, West, South
    }
}
