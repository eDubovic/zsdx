﻿using Assets.Zsdx.Scripts.Core.Movements;
using Assets.Zsdx.Scripts.View.Hero;
using Assets.Zsdx.Scripts.ZeldaApplication;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Core {
    public class Hero {

        private readonly Movement _movement;

        public Hero() {
            this._movement = new Movement(new Straight());
        }

        public void Walk() {
            
        }

        public void Move(HeroArgs heroArgs) {

             this._movement.Start(heroArgs);
        }
    }
}
