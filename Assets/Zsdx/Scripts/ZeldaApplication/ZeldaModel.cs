﻿using thelab.mvc;
using Assets.Zsdx.Scripts.Model;

namespace Assets.Zsdx.Scripts.ZeldaApplication {
    public class ZeldaModel: Model<ZeldaApplication> {

        private HeroModel _hero;
        public HeroModel Hero {
            get {
                return this._hero = this.Assert(this._hero, true);
            }
        }

        private ChestModel _chest;
        public ChestModel Chest {
            get {
                return this._chest = this.Assert(this._chest);
            }
        }
    }
}