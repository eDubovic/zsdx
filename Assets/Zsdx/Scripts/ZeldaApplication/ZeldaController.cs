﻿using thelab.mvc;
using Assets.Zsdx.Scripts.Controller;

namespace Assets.Zsdx.Scripts.ZeldaApplication {
    public class ZeldaController: Controller<ZeldaApplication> {

        private HeroController _hero;
        public HeroController Hero {
            get {
                return this._hero = this.Assert(this._hero);
            }

        }

        private ChestController _chest;
        public ChestController Chest {
            get {
                return this._chest = this.Assert(this._chest);
            }
        }
    }
}
