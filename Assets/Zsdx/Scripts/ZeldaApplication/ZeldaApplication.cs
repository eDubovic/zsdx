﻿using thelab.mvc;

namespace Assets.Zsdx.Scripts.ZeldaApplication {
    public class ZeldaApplication: BaseApplication<ZeldaModel, ZeldaView, ZeldaController> {

        public static ZeldaApplication Instance {
            get;
            private set;
        }

        private void Awake() {
            Instance = this;
        }
    }
}
