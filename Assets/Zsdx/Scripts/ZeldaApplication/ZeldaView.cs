﻿using thelab.mvc;
using Assets.Zsdx.Scripts.View.Hero;

namespace Assets.Zsdx.Scripts.ZeldaApplication {
    public class ZeldaView: View<ZeldaApplication> {

        private HeroView _hero;
        public HeroView Hero {
            get {
                return this._hero = this.Assert(this._hero);
            }
        }
    }
}