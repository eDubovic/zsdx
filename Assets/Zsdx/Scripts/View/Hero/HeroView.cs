﻿using Assets.Zsdx.Scripts.Core;
using Assets.Zsdx.Scripts.Core.HeroState;
using Assets.Zsdx.Scripts.Interfaces;
using thelab.mvc;
using UnityEngine;

namespace Assets.Zsdx.Scripts.View.Hero {
    public class HeroView: View<ZeldaApplication.ZeldaApplication>  {

        public Animator Animator { get; private set; }
        private IHeroState _currentHeroState;

        public void Start() {
            this.Animator = this.GetComponent<Animator>();
            this.ChangeState(new StoppedState());
        }

        public void Update() {
            this._currentHeroState.Execude();
        }

        public void ChangeState(IHeroState heroState) {
            if (this._currentHeroState != null) {
                this._currentHeroState.Exit();
            }
            this._currentHeroState = heroState;
            this._currentHeroState.Enter(this);
        }
    }
}
