﻿using Assets.Zsdx.Scripts.Core;

namespace Assets.Zsdx.Scripts.Items {
    public class Apple: Item {
        public Apple() {
            this.Name = "apple";
            this.Description = "Apples (x3)";
            this.RemainingUses = 3;
        }

        protected override bool UseItem() {
            return true;
        }
    }
}
