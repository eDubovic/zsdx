﻿using Assets.Zsdx.Scripts.Core;

namespace Assets.Zsdx.Scripts.Items {
    public class Shield: Item {
        public Shield() {
            this.Name = "shield";
            this.Description = "Shield";
            this.RemainingUses = 0;
        }

        protected override bool UseItem() {
            return true;
        }
    }
}
