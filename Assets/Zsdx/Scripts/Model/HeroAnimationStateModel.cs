﻿using thelab.mvc;
using Assets.Zsdx.Scripts.Core;

namespace Assets.Zsdx.Scripts.Model {
    public class HeroAnimationStateModel: Model<ZeldaApplication.ZeldaApplication> {
        public Direction Direction = Direction.East;
        public float WalkingSpeed = 10.0f;
        public bool IsWalking = false;
        public bool IsDiagonal = false;
        public float Horizontal = 0;
        public float Vertical = -1;


    }
}
