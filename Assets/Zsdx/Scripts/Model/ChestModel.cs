﻿using Assets.Zsdx.Scripts.Interfaces;
using thelab.mvc;
using UnityEngine;
using Application = Assets.Zsdx.Scripts.ZeldaApplication.ZeldaApplication;

namespace Assets.Zsdx.Scripts.Model {
    public class ChestModel: Model<Application> {
        public int Layer;
        public int X;
        public int Y;
        public string Name;
        public IItem Item;
        public string TreasureName;
        public int TreasureVariant;
        public string TreasureSaveGameVariable;
        public string Sprite;
        public string OpeningCondition;
        public string OpeningConditionConsumed;
        public string CannotOpenDialog;

    }
}
