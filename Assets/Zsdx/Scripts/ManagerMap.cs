﻿using System.Collections.Generic;
using System.IO;

using LitJson;
using UnityEngine;
using MapEntity = Assets.Zsdx.Scripts.Entity.Map.Map;

namespace Assets.Zsdx.Scripts {
    public class ManagerMap {

        private const int CountMap = 139;
        private readonly List<MapEntity> _maps;

        public ManagerMap() {
            this._maps = new List<MapEntity>();
            this.LoadMaps();
        }

        public MapEntity GetMapById(int id) {
            return this._maps.Find(x => x.Id.Equals(id));
        }

        private void LoadMaps() {
            for (int i = 0; i <= CountMap; i++) {
                string path = string.Format("{0}{1}", Application.dataPath, string.Format("/Resources/maps/{0}.json", i));
                if (!File.Exists(path)) {
                    continue;
                }
                this._maps.Add(JsonMapper.ToObject<MapEntity>(File.ReadAllText(path)));
            }
        }
    }
}
