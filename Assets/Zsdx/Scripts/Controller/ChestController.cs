﻿using UnityEngine;
using thelab.mvc;
using Application = Assets.Zsdx.Scripts.ZeldaApplication.ZeldaApplication;

namespace Assets.Zsdx.Scripts.Controller {
    public class ChestController: Controller<Application> {

        public override void OnNotification(string pEvent, Object pTarget, params object[] pData) {

            switch (pEvent) {
                case "scene.load":
                    this.app.model.Chest.Name = "Name";
                    Debug.Log("scene");
                    break;
            }
        }

        public void OnOpened(int treasureItem, int treasureVariant, string treasureSavegameVariable) {
               Debug.Log("Chest");
        }

    }
}