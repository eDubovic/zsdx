﻿using System;
using UnityEngine;
using thelab.mvc;
using Assets.Zsdx.Scripts.Core;
using Assets.Zsdx.Scripts.Core.HeroState;
using Object = UnityEngine.Object;

namespace Assets.Zsdx.Scripts.Controller {
    public class HeroController: Controller<ZeldaApplication.ZeldaApplication> {

        private readonly Hero _hero = new Hero();
        private readonly HeroArgs _heroArgs = new HeroArgs();

        private Animator _animator;

        public override void OnNotification(string pEvent, Object pTarget, params object[] pData) {
            Debug.Log("pEvent = " + pEvent);

            switch (pEvent) {
                case HeroNotifications.StoppedAnimatorEnter:
                    Debug.Log("StoppedAnimatorEnter");
                break;
                default:
                break;
            }

        }


        public void Start() {
            this._animator = this.app.view.Hero.Animator;
        }

        public void Update() {
            float horizontal = Input.GetAxisRaw("Horizontal");
            float vertical = Input.GetAxisRaw("Vertical");

            this.app.model.Hero.IsWalking = (Mathf.Abs(horizontal) + Mathf.Abs(vertical)) > 0;
            this.app.model.Hero.IsDiagonal = (int)horizontal != 0 && (int)vertical != 0;

            if (this.app.model.Hero.IsWalking) {
                this.app.model.Hero.Horizontal = horizontal;
                this.app.model.Hero.Vertical = vertical;
                this._heroArgs.Transform = this.app.view.Hero.transform;
                this._heroArgs.WalkingSpeed = this.app.model.Hero.WalkingSpeed;
                this._heroArgs.X = this.app.model.Hero.Horizontal;
                this._heroArgs.Y = this.app.model.Hero.Vertical;
                this._hero.Move(this._heroArgs);
            }

            //Horizontal left < 0
            //Horizontal right > 0
            //Vertical up > 0
            //Vertical down < 0


            
        }
    }
}
