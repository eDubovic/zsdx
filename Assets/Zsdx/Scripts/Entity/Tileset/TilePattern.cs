﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Entity.Tileset {

    public class TilePattern {
        public int Id { get; set; }
        public string Ground { get; set; }
        public int DefaultLayer { get; set; }
        public int[] X { get; set; }
        public int[] Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Scrolling { get; set; }
        public string RepeatMode { get; set; }
        public List<Sprite> Sprite { get; set; }
    }
}
