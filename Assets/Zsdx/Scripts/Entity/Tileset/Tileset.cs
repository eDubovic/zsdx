﻿using System.Collections.Generic;
using UnityEngine;

namespace Assets.Zsdx.Scripts.Entity.Tileset {

    /// <summary>
    /// Tileset
    /// </summary>
    public class Tileset {

        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Background color
        /// </summary>
        public int[] BackgroundColor { get; set; }

        /// <summary>
        /// TilePattern
        /// </summary>
        public List<TilePattern> TilePattern { get; set; }

        public Texture2D Texture2D { get; set; }
    }
}
