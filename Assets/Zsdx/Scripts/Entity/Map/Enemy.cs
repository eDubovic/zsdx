namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Enemy
    {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public int Direction { get; set; }
        public string Breed { get; set; }
        public string TreasureName { get; set; }
        public int TreasureVariant { get; set; }
    }
}
