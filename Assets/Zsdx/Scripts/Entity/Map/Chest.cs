﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    //
    // A chest is a box that contains a treasure.
    //
    public class Chest {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public string TreasureName { get; set; }
        public int TreasureVariant { get; set; }
        public string TreasureSaveGameVariable { get; set; }
        public string Sprite { get; set; }
        public string OpeningCondition { get; set; }
        public string OpeningConditionConsumed { get; set; }
        public string CannotOpenDialog { get; set; }
    }
}
