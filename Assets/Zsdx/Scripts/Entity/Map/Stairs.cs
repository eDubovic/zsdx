﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Stairs
    {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public int Direction { get; set; }
        public int Subtype { get; set; }
    }
}
