﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Pickable {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string TreasureName { get; set; }
        public int TreasureVariant { get; set; }
        public string TreasureSaveGameVariable { get; set; }
    }
}
