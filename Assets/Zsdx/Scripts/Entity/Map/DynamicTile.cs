﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class DynamicTile {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public int Pattern { get; set; }
        public bool EnabledAtStart { get; set; }
    }
}
