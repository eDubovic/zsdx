namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Switch
    {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public string Subtype { get; set; }
        public string Sprite { get; set; }
        public string Sound { get; set; }
        public bool NeedsBlock { get; set; }
        public bool InactivateWhenLeaving { get; set; }
    }
}
