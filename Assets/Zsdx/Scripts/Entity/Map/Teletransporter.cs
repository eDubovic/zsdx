﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Teletransporter {
        public string Name { get; set; }
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Sprite { get; set; }
        public string Sound { get; set; }
        public string Transition { get; set; }
        public int DestinationMap { get; set; }
        public string Destination { get; set; }
    }
}
