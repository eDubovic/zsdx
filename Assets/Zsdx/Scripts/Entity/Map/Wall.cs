namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Wall
    {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public bool StopsHero { get; set; }
        public bool StopsEnemies { get; set; }
        public bool StopsNpcs { get; set; }
        public bool StopsBlocks { get; set; }
    }
}
