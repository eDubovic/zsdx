﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Block
    {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public int Direction { get; set; }
        public string Sprite { get; set; }
        public bool Pushable { get; set; }
        public bool Pullable { get; set; }
        public int MaximumMoves { get; set; }
    }
}
