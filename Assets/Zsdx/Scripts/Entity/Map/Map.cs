﻿using System.Collections.Generic;

namespace Assets.Zsdx.Scripts.Entity.Map {
    /// <summary>
    /// Map
    /// </summary>
    public class Map {
        public int Id { get; set; }
        public string Description { get; set; }
        public Properties Properties { get; set; }
        public List<Door> Door { get; set; }
        public List<DynamicTile> DynamicTile { get; set; }
        public List<Npc> Npc { get; set; }
        public List<Destination> Destination { get; set; }
        public List<Teletransporter> Teletransporter { get; set; }
        public List<Chest> Chest { get; set; }
        public List<Destructible> Destructible { get; set; }
        public List<Tile> Tile { get; set; }
        public List<Stairs> Stairs { get; set; }
        public List<Sensor> Sensor { get; set; }
        public List<Pickable> Pickable { get; set; }
        public List<Block> Block { get; set; }
        public List<Jumper> Jumper { get; set; }
        public List<Wall> Wall { get; set; }
        public List<Switch> Switch { get; set; }
        public List<Enemy> Enemy { get; set; }
    }
}
