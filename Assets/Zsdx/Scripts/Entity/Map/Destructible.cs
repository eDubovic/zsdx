﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Destructible {
        public string Name { get; set; }
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string TreasureName { get; set; }
        public int TreasureVariant { get; set; }
        public string TreasureSaveGameVariable { get; set; }
        public string Sprite { get; set; }
        public string DestructionSound { get; set; }
        public int Weight { get; set; }
        public bool CanBeCut { get; set; }
        public bool CanExplode { get; set; }
        public bool CanRegenerate { get; set; }
        public int DamageOnEnemies { get; set; }
        public string Ground { get; set; }
    }
}
