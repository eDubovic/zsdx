﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    /// <summary>
    /// Door
    /// </summary>
    public class Door {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public string Name { get; set; }
        public int Direction { get; set; }
        public string Sprite { get; set; }
        public string SaveGameVariable { get; set; }
        public string OpeningMethod { get; set; }
        public string OpeningCondition { get; set; }
        public bool OpeningConditionConsumed { get; set; }
        public string CannotOpenDialog { get; set; }
    }
}
