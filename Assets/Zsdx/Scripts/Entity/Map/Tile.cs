﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Tile {
        public int Layer { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Pattern { get; set; }
    }
}
