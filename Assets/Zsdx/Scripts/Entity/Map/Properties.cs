﻿namespace Assets.Zsdx.Scripts.Entity.Map {
    /// <summary>
    /// Properties
    /// </summary>
    public class Properties {
        /// <summary>
        /// X coordinate of the top-left corner of the map in its world. Useful to store the location of this map if it belongs to a group of adjacent maps. The engine uses this information to implement scrolling between two adjacent maps. The default value is 0. See map:get_location() for more details.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Y coordinate of the top-left corner of the map in it world. The default is 0.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Width of the map in pixels. Must be a multiple of 8.
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Height of the map in pixels. Must be a multiple of 8.
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// A name that identifies a group of maps this map belongs to. Worlds allow to group maps together. The world can be any arbitrary name. Maps that have the same non-empty world name are considered to be part of the same environment. The starting location of the savegame is automatically set by the engine when the world changes (not when the map changes). See map:get_world() for more details.
        /// </summary>
        public string World { get; set; }

        /// <summary>
        /// The floor of this map if it is part of a floor system. This property is optional. The engine does not do anything particular with the floor, but your quest can use it in scripts, for example to show the current floor on the HUD when it changes or to make a minimap menu. 0 is the first floor, 1 is the second floor, -1 is the first basement floor, etc.
        /// </summary>
        public int Floor { get; set; }

        /// <summary>
        ///  Index of the lowest layer (0 or less).
        /// </summary>
        public int MinLayer { get; set; }

        /// <summary>
        /// Index of the highest layer (0 or less).
        /// </summary>
        public int MaxLayer { get; set; }

        /// <summary>
        /// Id of the tileset to use as a skin for this map.
        /// </summary>
        public int Tileset { get; set; }

        /// <summary>
        /// Id of the music to play when entering this map.It can be a music file name relative to the "musics" directory (without extension), or the special value "none" to play no music on this map, or the special value "same" to keep the music unchanged. No value means no music.
        /// </summary>
        public string Music { get; set; }

    }
}
