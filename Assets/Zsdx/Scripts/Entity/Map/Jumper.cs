namespace Assets.Zsdx.Scripts.Entity.Map {
    public class Jumper
    {
        public int Layer { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
        public int Direction { get; set; }
        public int JumpLength { get; set; }
    }
}
