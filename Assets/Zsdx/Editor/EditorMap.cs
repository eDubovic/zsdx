﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Assets.Zsdx.Scripts;

using MapLib = Assets.Zsdx.Scripts.Lib.Map;
using MapEntity = Assets.Zsdx.Scripts.Entity.Map.Map;
using TilesetEntity = Assets.Zsdx.Scripts.Entity.Tileset.Tileset;

namespace Assets.Zsdx.Editor {
    public class EditorMap: EditorWindow {

        private ManagerMap _managerMap = null;
        private ManagerTileset _managerTileset = null;
        private MapLib _mapLib = null;

        [MenuItem("Zsdx/Map/Generate")]
        private static void Init() {
            EditorMap window = (EditorMap)GetWindow(typeof(EditorMap));
            window.Show();
        }

        private void OnGUI() {
            GUILayout.Label("Map", EditorStyles.boldLabel);
            
            if (GUILayout.Button("Generate")) {
                this._managerMap = new ManagerMap();
                this._managerTileset = new ManagerTileset();
                this._mapLib = new MapLib();

                MapEntity map = this._managerMap.GetMapById(28);
                TilesetEntity tileset = this._managerTileset.FindTilesetByMap(map);

                this._mapLib.CreateMap(map, tileset);
            } 
        }

    }
}
